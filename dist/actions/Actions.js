'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _dgxAltCenter = require('dgx-alt-center');

var _dgxAltCenter2 = _interopRequireDefault(_dgxAltCenter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Actions = function () {
  function Actions() {
    _classCallCheck(this, Actions);
  }

  _createClass(Actions, [{
    key: 'setMobileMenuButtonValue',
    value: function setMobileMenuButtonValue(currentActiveMobileButton) {
      return currentActiveMobileButton;
    }
  }, {
    key: 'setMobileMyNyplButtonValue',
    value: function setMobileMyNyplButtonValue(value) {
      return value;
    }
  }, {
    key: 'searchButtonActionValue',
    value: function searchButtonActionValue(actionValue) {
      return actionValue;
    }
  }, {
    key: 'updateIsHeaderSticky',
    value: function updateIsHeaderSticky(value) {
      return value;
    }
  }, {
    key: 'toggleSubscribeFormVisible',
    value: function toggleSubscribeFormVisible(value) {
      return value;
    }
  }, {
    key: 'toggleMyNyplVisible',
    value: function toggleMyNyplVisible(value) {
      return value;
    }
  }, {
    key: 'toggleStickyMyNyplVisible',
    value: function toggleStickyMyNyplVisible(value) {
      return value;
    }
  }]);

  return Actions;
}();

exports.default = _dgxAltCenter2.default.createActions(Actions);
module.exports = exports['default'];