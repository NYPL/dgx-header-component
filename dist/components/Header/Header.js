'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.navConfig = exports.Header = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _underscore = require('underscore');

var _dgxFeatureFlags = require('dgx-feature-flags');

var _dgxFeatureFlags2 = _interopRequireDefault(_dgxFeatureFlags);

var _navConfig = require('../../navConfig.js');

var _navConfig2 = _interopRequireDefault(_navConfig);

var _featureFlagConfig = require('../../featureFlagConfig.js');

var _featureFlagConfig2 = _interopRequireDefault(_featureFlagConfig);

var _appConfig = require('../../appConfig.js');

var _appConfig2 = _interopRequireDefault(_appConfig);

var _HeaderStore = require('../../stores/HeaderStore.js');

var _HeaderStore2 = _interopRequireDefault(_HeaderStore);

var _Actions = require('../../actions/Actions.js');

var _Actions2 = _interopRequireDefault(_Actions);

var _Logo = require('../Logo/Logo.js');

var _Logo2 = _interopRequireDefault(_Logo);

var _DonateButton = require('../DonateButton/DonateButton.js');

var _DonateButton2 = _interopRequireDefault(_DonateButton);

var _SimpleLink = require('../Links/SimpleLink.js');

var _SimpleLink2 = _interopRequireDefault(_SimpleLink);

var _SubscribeButton = require('../SubscribeButton/SubscribeButton.js');

var _SubscribeButton2 = _interopRequireDefault(_SubscribeButton);

var _MyNyplButton = require('../MyNyplButton/MyNyplButton.js');

var _MyNyplButton2 = _interopRequireDefault(_MyNyplButton);

var _NavMenu = require('../NavMenu/NavMenu.js');

var _NavMenu2 = _interopRequireDefault(_NavMenu);

var _MobileHeader = require('./MobileHeader.js');

var _MobileHeader2 = _interopRequireDefault(_MobileHeader);

var _GlobalAlerts = require('../GlobalAlerts/GlobalAlerts.js');

var _GlobalAlerts2 = _interopRequireDefault(_GlobalAlerts);

var _FundraisingBanner = require('../FundraisingBanner/FundraisingBanner');

var _FundraisingBanner2 = _interopRequireDefault(_FundraisingBanner);

var _dgxSkipNavigationLink = require('dgx-skip-navigation-link');

var _dgxSkipNavigationLink2 = _interopRequireDefault(_dgxSkipNavigationLink);

var _utils = require('../../utils/utils.js');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // NPM Modules

// Feature FeatureFlags


// Nav Config

// ALT Flux

// NYPL Components

// Utility Library


var styles = {
  wrapper: {
    position: 'relative'
  },
  topButtons: {
    position: 'absolute',
    top: '20px',
    textTransform: 'uppercase',
    display: 'block'
  },
  locationsTopLink: {
    display: 'inline-block',
    color: '#000',
    backgroundColor: '#FFF',
    padding: '5px 15px 5px 5px',
    verticalAlign: 'baseline'
  },
  libraryCardButton: {
    display: 'inline-block',
    color: '#000',
    backgroundColor: '#FFF',
    padding: '5px',
    verticalAlign: 'baseline'
  },
  subscribeButton: {
    display: 'inline-block',
    margin: '0px 10px 0px 0px',
    verticalAlign: 'baseline'
  },
  donateButton: {
    display: 'inline-block',
    padding: '10px 18px',
    margin: '0 5px 0 0',
    lineHeight: 'normal',
    verticalAlign: 'baseline'
  },
  shopLink: {
    color: '#000',
    backgroundColor: '#FFF',
    padding: '10px 15px',
    margin: '0 0 0 5px',
    verticalAlign: 'baseline'
  },
  mobileMyNypl: {
    position: 'absolute',
    zIndex: 1000,
    right: '0',
    width: '220px',
    minHeight: '130px',
    backgroundColor: '#1B7FA7',
    padding: '25px 30px'
  }
};

var Header = function (_React$Component) {
  _inherits(Header, _React$Component);

  function Header(props) {
    _classCallCheck(this, Header);

    var _this = _possibleConstructorReturn(this, (Header.__proto__ || Object.getPrototypeOf(Header)).call(this, props));

    var _this$props = _this.props,
        patron = _this$props.patron,
        navData = _this$props.navData;

    var patronNameObject = !(0, _underscore.isEmpty)(patron) && patron.names && patron.names.length ? _utils2.default.modelPatronName(patron.names[0]) : {};

    _this.state = (0, _underscore.extend)({
      headerHeight: null,
      navData: navData,
      loginCookieName: 'nyplIdentityPatron',
      loginCookieValue: null,
      patronName: patronNameObject.name || '',
      patronInitial: patronNameObject.initial || '',
      patronDataReceived: patron.loggedIn || false,
      isFeatureFlagsActivated: {},
      logOutUrl: ''
    }, _HeaderStore2.default.getState(), { featureFlagsStore: _dgxFeatureFlags2.default.store.getState() });

    _this.handleStickyHeader = _this.handleStickyHeader.bind(_this);
    return _this;
  }

  _createClass(Header, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      _HeaderStore2.default.listen(this.onChange.bind(this));
      // Listen on FeatureFlags Store updates
      _dgxFeatureFlags2.default.store.listen(this.onFeatureFlagsChange.bind(this));
      // Height needs to be set once the alerts (if any) are mounted.
      this.setHeaderHeight();
      // Listen to the scroll event for the sticky header.
      window.addEventListener('scroll', this.handleStickyHeader, false);
      // Set the log out link to state
      this.setLogOutLink(window.location.href);
      // Set nyplIdentityPatron cookie to the state.
      this.setLoginCookie(this.state.loginCookieName);
      // Set feature flag cookies to the state
      // We don't have any feature flags set in the config list at this moment though
      _utils2.default.checkFeatureFlagActivated(_featureFlagConfig2.default.featureFlagList, this.state.isFeatureFlagsActivated);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      _HeaderStore2.default.unlisten(this.onChange.bind(this));
      // Listen on FeatureFlags Store updates
      _dgxFeatureFlags2.default.store.unlisten(this.onFeatureFlagsChange.bind(this));
      // Removing event listener to minimize garbage collection
      window.removeEventListener('scroll', this.handleStickyHeader, false);
    }
  }, {
    key: 'onChange',
    value: function onChange() {
      this.setState((0, _underscore.extend)({
        headerHeight: this.state.headerHeight,
        loginCookieValue: this.state.loginCookieValue,
        patronName: this.state.patronName,
        patronInitial: this.state.patronInitial,
        patronDataReceived: this.state.patronDataReceived,
        isFeatureFlagsActivated: {},
        logOutUrl: this.state.logOutUrl
      }, _HeaderStore2.default.getState()));
    }
  }, {
    key: 'onFeatureFlagsChange',
    value: function onFeatureFlagsChange() {
      this.setState({ featureFlagsStore: _dgxFeatureFlags2.default.store.getState() });
    }

    /**
     * setLoginCookie()
     * Updates the state loginCookieValue property
     */

  }, {
    key: 'setLoginCookie',
    value: function setLoginCookie(cookie) {
      if (_utils2.default.hasCookie(cookie)) {
        var loginCookieValue = _utils2.default.getCookie(cookie);

        this.setState({ loginCookieValue: loginCookieValue });

        if (!this.state.patronDataReceived) {
          this.fetchPatronData(loginCookieValue);
        }
      } else {
        this.setState({ loginCookieValue: null });
      }
    }

    /**
     * getHeaderHeight()
     * returns the Height of the Header DOM
     * element in pixels.
     */

  }, {
    key: 'getHeaderHeight',
    value: function getHeaderHeight() {
      var headerDOM = _reactDom2.default.findDOMNode(this.refs.nyplHeader);
      return headerDOM.getBoundingClientRect().height;
    }

    /**
     * setHeaderHeight()
     * Updates the state headerHeight property
     * only if headerHeight is not defined.
     */

  }, {
    key: 'setHeaderHeight',
    value: function setHeaderHeight() {
      var _this2 = this;

      if (!this.state.headerHeight) {
        setTimeout(function () {
          _this2.setState({ headerHeight: _this2.getHeaderHeight() });
        }, 500);
      }
    }

    /**
     * getWindowVerticallScroll()
     * returns the current window vertical
     * scroll position in pixels.
     * @returns {number} - Vertical Scroll Position.
     */

  }, {
    key: 'getWindowVerticalScroll',
    value: function getWindowVerticalScroll() {
      return window.scrollY || window.pageYOffset || document.documentElement.scrollTop;
    }

    /**
     * setLogOutLink(location)
     * Generate the full log out url including the redirect URI, and update the state with it.
     * @param {location} - The URI for redirect request
     */

  }, {
    key: 'setLogOutLink',
    value: function setLogOutLink(location) {
      this.setState({ logOutUrl: _utils2.default.renderDynamicLogOutLink(location) });
    }

    /**
     * fetchPatronData(cookie)
     * Executes utils.getLoginData to fetch patron's data based on the cookie.
     * Updates the state with the results.
     * Also, pass this.setLoginCookie(), if cookie needs to be refreshed and set again.
     * @param {cookie} - The cookie returned from log in.
     */

  }, {
    key: 'fetchPatronData',
    value: function fetchPatronData(cookie) {
      var _this3 = this;

      _utils2.default.getLoginData(cookie, function (result) {
        if (result.data && result.data.data) {
          var patronNameObject = _utils2.default.modelPatronName(_utils2.default.extractPatronName(result.data));

          _this3.setState({
            patronName: patronNameObject.name,
            patronInitial: patronNameObject.initial,
            patronDataReceived: true
          });
        }
      }, _appConfig2.default.loginMyNyplLinks.tokenRefreshLink, function () {
        _this3.setLoginCookie(_this3.state.loginCookieName);
      });
    }

    /**
     * handleStickyHeader()
     * Executes Actions.updateIsHeaderSticky()
     * with the proper boolean value to update the
     * HeaderStore.isSticky value based on the window
     * vertical scroll position surpassing the height
     * of the Header DOM element.
     */

  }, {
    key: 'handleStickyHeader',
    value: function handleStickyHeader() {
      var headerHeight = this.state.headerHeight;
      var windowVerticalDistance = this.getWindowVerticalScroll();

      if (windowVerticalDistance && headerHeight && windowVerticalDistance > headerHeight) {
        // Only update the value if sticky is false
        if (!_HeaderStore2.default.getIsStickyValue()) {
          // Fire GA Event when Header is in Sticky Mode
          _utils2.default.trackHeader.bind(this, 'scroll', 'Sticky Header');
          // Update the isSticky flag
          _Actions2.default.updateIsHeaderSticky(true);
        }
      } else {
        // Avoids re-assignment on each scroll by checking if it is already true
        if (_HeaderStore2.default.getIsStickyValue()) {
          _Actions2.default.updateIsHeaderSticky(false);
        }
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var isHeaderSticky = this.state.isSticky;
      var headerHeight = this.state.headerHeight;
      var headerClass = this.props.className || 'Header';
      var headerClasses = (0, _classnames2.default)(headerClass, { sticky: isHeaderSticky });
      var skipNav = this.props.skipNav ? _react2.default.createElement(_dgxSkipNavigationLink2.default, this.props.skipNav) : '';
      var isLoggedIn = !!this.state.patronDataReceived;
      var gaAction = isLoggedIn ? 'My Account' : 'Log In';

      return _react2.default.createElement(
        'header',
        {
          id: this.props.id,
          className: headerClasses,
          ref: 'nyplHeader',
          style: isHeaderSticky ? { height: headerHeight + 'px' } : null
        },
        skipNav,
        _react2.default.createElement(_GlobalAlerts2.default, { className: headerClass + '-GlobalAlerts' }),
        _react2.default.createElement(
          'div',
          { className: headerClass + '-Wrapper' },
          _react2.default.createElement(_MobileHeader2.default, {
            className: headerClass + '-Mobile',
            locatorUrl: this.props.urlType === 'absolute' ? '//www.nypl.org/locations/map?nearme=true' : '/locations/map?nearme=true',
            nyplRootUrl: this.props.urlType === 'absolute' ? '//www.nypl.org' : '/',
            isLoggedIn: isLoggedIn,
            patronName: this.state.patronName,
            logOutLink: this.state.logOutUrl,
            ref: 'headerMobile'
          }),
          _react2.default.createElement(
            'div',
            {
              className: headerClass + '-TopWrapper',
              style: styles.wrapper,
              ref: 'headerTopWrapper'
            },
            _react2.default.createElement(_Logo2.default, {
              className: headerClass + '-Logo',
              target: this.props.urlType === 'absolute' ? '//www.nypl.org' : '/'
            }),
            _react2.default.createElement(
              'nav',
              {
                className: headerClass + '-Buttons',
                style: styles.topButtons,
                'aria-label': 'Header top links'
              },
              _react2.default.createElement(
                'ul',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(_MyNyplButton2.default, {
                    refId: 'desktopLogin',
                    isLoggedIn: isLoggedIn,
                    patronName: this.state.patronName,
                    logOutLink: this.state.logOutUrl,
                    gaAction: gaAction
                  })
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(_SimpleLink2.default, {
                    label: 'Locations',
                    target: this.props.urlType === 'absolute' ? '//www.nypl.org/locations/map' : '/locations/map',
                    className: 'LocationsTopLink',
                    id: 'LocationsTopLink',
                    gaAction: 'Locations',
                    gaLabel: 'Header Top Links',
                    style: styles.locationsTopLink
                  })
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(_SimpleLink2.default, {
                    label: 'Get a Library Card',
                    target: this.props.urlType === 'absolute' ? '//www.nypl.org/library-card' : '/library-card',
                    className: 'LibraryCardButton',
                    id: 'LibraryCardButton',
                    gaAction: 'Get a Library Card',
                    gaLabel: 'Header Top Links',
                    style: styles.libraryCardButton
                  })
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(_SubscribeButton2.default, {
                    label: 'Get Email Updates',
                    lang: this.props.lang,
                    style: styles.subscribeButton
                  })
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(_DonateButton2.default, {
                    id: 'Top-DonateButton',
                    lang: this.props.lang,
                    style: styles.donateButton,
                    gaLabel: 'Header Top Links'
                  })
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(_SimpleLink2.default, {
                    label: 'Shop',
                    target: 'http://shop.nypl.org/?utm_campaign=NYPLHeaderButton&utm_' + 'source=nypl.org&utm_medium=referral',
                    className: 'shopTopLink',
                    id: 'shopTopLink',
                    gaAction: 'Shop',
                    gaLabel: 'Header Top Links',
                    style: styles.shopLink
                  })
                )
              )
            )
          ),
          _react2.default.createElement(_NavMenu2.default, {
            className: headerClass + '-NavMenu',
            lang: this.props.lang,
            items: this.state.navData,
            urlType: this.props.urlType,
            isLoggedIn: isLoggedIn,
            patronName: this.state.patronName,
            logOutLink: this.state.logOutUrl,
            gaAction: gaAction
          })
        ),
        _dgxFeatureFlags2.default.store._isFeatureActive(_appConfig2.default.fundraising.experimentName) && _react2.default.createElement(_FundraisingBanner2.default, {
          hideBannerCookieName: 'closeFundraiserBanner',
          gaLabel: 'Header Fundraising Banner'
        })
      );
    }
  }]);

  return Header;
}(_react2.default.Component);

Header.propTypes = {
  lang: _propTypes2.default.string,
  className: _propTypes2.default.string,
  id: _propTypes2.default.string,
  navData: _propTypes2.default.array,
  skipNav: _propTypes2.default.object,
  patron: _propTypes2.default.object,
  urlType: _propTypes2.default.string
};

Header.defaultProps = {
  lang: 'en',
  className: 'Header',
  id: 'nyplHeader',
  skipNav: null,
  urlType: '',
  patron: {}
};

exports.Header = Header;
exports.navConfig = _navConfig2.default;